#!/bin/bash
# Set target host IP or hostname
TARGET_HOST='130.60.145.1'

count=$(ping -c 5 -i 300 $TARGET_HOST | grep from* | wc -l)
if [ $count -eq 0 ]; then
    echo "$(date)" "Target host" $TARGET_HOST "unreachable, Rebooting!" >>/var/log/restart_no_ping.log
    /sbin/shutdown -r now

# else
#    echo "$(date) ===-> OK! " >>/var/log/restart_no_ping.log
fi