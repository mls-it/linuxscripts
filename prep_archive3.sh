#!/bin/bash


date=$(date +"%Y-%m-%d")
date_month=$(date +"%Y-%m")
date_hour=$(date +"%Y-%m-%d  %H:%M:%S")
# Function for the process of each group
tar_and_copy () {

        # only execute if depth parameter is set
        if [ $# -eq 1 ]; then

                for ((i=0;i<=$1;i++))
                        do
                        # Rename Folders with spaces  (t e s t  --> t_e_s_t )
                        find /mnt/G_DQBM_BB_RAW/$cytof/  -maxdepth $i -name '*.zip' -mmin +525600 -daystart -size +1k | rename 's/\s/_/g'
                done

                                # create parent directory if it doesn't exist
                                if [ ! -f /mnt/archiv_bb/10y/$cytof/$date_month.tar ]; then

                                #create new folder on share if it doesnt exist
                                if [ ! -f /mnt/G_DQBM_BB_RAW/$cytof/$date_month ]; then

                                mkdir /mnt/G_DQBM_BB_RAW/$cytof/$date_month

                                fi
                                # move all files to folder
                                find /mnt/G_DQBM_BB_RAW/$cytof -maxdepth $1 -name '*.zip*' -mmin +525600 -daystart -size +1k -exec mv {} /mnt/G_DQBM_BB_RAW/$cytof/$date_month \; -exec echo '{}' >> ${date_month}_${cytof}_content_log.txt \;



                                # tar folder at once (no r option) --> check performance
                                tar -cvf /data/bodenmiller/10y/$cytof/$date_month.tar /mnt/G_DQBM_BB_RAW/$cytof/$date_month/



                                # tar the files into /test/.... and remove them if completed successfull
                             #   find /mnt/G_DQBM_BB_RAW/$cytof -maxdepth $1 -name '*.zip*' -mmin +525600 -daystart -size +1k -exec tar -rvf /data/bodenmiller/10y/$cytof/$date_month.tar '{}' \; <-- Slow because it checks the whole tar for duplicates before adding next file??
                                # Error Check if tar successfull
                                        if [ $? -eq 0 ]; then
                                        # Log file with Sucess message
                                        echo [SUCCESS]  [ ${date_hour} ]  Tar archive $cytof/$date_month.tar completed with no errors! >> ${date}_${cytof}_tar_log.txt

                                        echo Tar log from ${date} ${cytof} - SUCCESS | mail -s "Tar log ${date} ${cytof} - SUCCESS" marco.schmidli@mls.uzh.ch,fabio.snozzi@mls.uzh.ch -A ${date}_${cytof}_tar_log.txt

                                        # Delete zip from G_DQBM_BB_RAW
                                       # todo UNCOMMENT LATER ON
                                       # find /mnt/G_DQBM_BB_RAW/$cytof/ -maxdepth $1 -name "*.zip*" -mmin +525600 -daystart -delete
                                        echo "deleting files"
                                        # Move Tar to Archive
                                        rsync -vrt --remove-source-files  /data/bodenmiller/10y/$cytof/$date_month.tar /mnt/archiv_bb/10y/$cytof

                                        rsync -vrt --remove-source-files  /data/central/$date_month.tar /mnt/archiv_bb/10y/Central


                                                if [ $? -eq 0 ]; then
                                                 echo [SUCCESS]  [ ${date_hour} ]  All files from $cytof copied successfull! >> ${date}_${cytof}_move_log.txt
                                                 echo Move log from ${date} ${cytof} - SUCCESS | mail -s "Move log ${date} ${cytof} - SUCCESS" marco.schmidli@mls.uzh.ch,fabio.snozzi@mls.uzh.ch -A ${date}_${cytof}_move_log.txt
                                                else
                                                 echo [ERROR]  [ ${date_hour} ]  $cytof had error during copy job! >> ${date}_move_log.txt
                                                 echo Move log from ${date} ${cytof} - ERROR | mail -s "Move log ${date} ${cytof} - ERROR" marco.schmidli@mls.uzh.ch,fabio.snozzi@mls.uzh.ch -A ${date}_${cytof}_move_log.txt
                                                fi
                                        else
                                                 echo [ERROR]  [ ${date_hour} ]  $cytof had error during TAR job! >> ${date}_tar_log.txt
                                                 echo Tar log from ${date} ${cytof} - ERROR | mail -s "Tar log ${date} ${cytof} - ERROR" marco.schmidli@mls.uzh.ch,fabio.snozzi@mls.uzh.ch -A ${date}_${cytof}_tar_log.txt
                                        fi

                                else
                                        echo [File already exists] [ ${date_hour} ] Tar archive /mnt/archiv_bb/10y/$cytof/$date_month.tar.gz >> ${date}_${cytof}_tar_log.txt
                                        echo [Error] Tar already exists:  /mnt/archive_bb/10y/$cytof/$date_month.tar | mail -s "Tar already exists ${cytof} - ERROR" marco.schmidli@mls.uzh.ch,fabio.snozzi@mls.uzh.ch

                                fi

        fi
}


# Loop through each directory and tar it
for directory in `find /mnt/G_DQBM_BB_RAW/ -maxdepth 1 -type d`
        do
        # define Filename and Directoryname
                cytof=$(basename $directory)
                case $cytof in
                        Gadolin_Data)
                                tar_and_copy 1
                        ;;
                        Eowyn_Data)
                                tar_and_copy 1
                        ;;
                        Miraculix_Data)
                                tar_and_copy 1
                        ;;
                        Tigerlilly_Data)
                                tar_and_copy 1
                        ;;
                        Zoidberg_Data)
                                tar_and_copy 1
                        ;;

                esac
done
