#!/bin/bash


date=$(date +"%Y-%m-%d")
date_hour=$(date +"%Y-%m-%d  %H:%M:%S")
# Function for the process of each group
tar_and_copy () {

	# only execute if depth parameter is set
	if [ $# -eq 2 ]; then

		for ((i=0;i<=$1;i++))
			do

			# Rename Folders with spaces  (t e s t  --> t_e_s_t )
			find /imls-fs003/$group/*/  -maxdepth $i -name "* *" -type d | rename 's/\s/_/g'
		done
		
	
		# loop through the directories specified with the depth parameter
		for to_tar_directory in `find /imls-fs003/$group/*/ -mindepth $1 -maxdepth $1 -type d`
			do


				# set variables for directory name and path
				tar_directory_path=$(dirname $to_tar_directory)
				tar_directory=$(basename $to_tar_directory)

#				echo $tar_directory
#				echo $tar_directory_path


					# determine the folderstrcuture
					path01=${tar_directory_path#*/}
					path02=${path01#*/}
					path03=${path02#*/}

				# create parent directory if it doesn't exist
#        			if [ ! -d /test/$tar_directory_path ]; then
#        				mkdir -p /test/$tar_directory_path
#      				fi



				# create parent directory if it doesn't exist
        			if [ ! -d /imls-artico/$2/$path03 ]; then
       					mkdir -p /imls-artico/$2/$path03

				fi

				if [ ! -f /imls-artico/$2/$path03/$tar_directory.tar.gz ]; then


				# tar the files into /test/.... and remove them if completed successfull
#			        tar -zcf /test/$tar_directory_path/$tar_directory.tar.gz  -C $tar_directory_path $tar_directory --remove-files




				        tar -vuf /imls-artico/$2/$path03/$tar_directory.tar.gz  -C $tar_directory_path $tar_directory --remove-files




				# Error Check if tar successfull
					if [ $? -eq 0 ]; then

					# Log file with Sucess message
						echo [SUCCESS]  [ ${date_hour} ]  Tar archive $tar_directory_path/$tar_directory completed with no errors! >> ${date}_tar_log.txt



					fi


	# Delete folder from imls-fs003 (should already be gone)


#					rm -rf $to_tar_directory*



					# Move Tar to Archive




				else
					echo [File already exists] [ ${date_hour} ] Tar archive /imls-artico/$2/$path03/$tar_directory.tar.gz >> ${date}_tar_log.txt
					echo [Error] Tar already exists:  /imls-artico/$2/$path03/$tar_directory.tar.gz | mail -s "Tar already exists" marco.schmidli@imls.uzh.ch,fabio.snozzi@imls.uzh.ch

				fi

		done
			chmod -R +w /imls-fs003/$group/

			echo "hallo Du $group"
			#cp -r /imls-fs003/$group/* /imls-artico/$2/
			rsync -vrt --remove-source-files  --exclude 'Thumbs.db' /imls-fs003/$group/ /imls-artico/$2/

					# IF / Else
			if [ $? -eq 0 ]; then
				 echo [SUCCESS]  [ ${date_hour} ]  All files from $group copied successfull! >> ${date}_move_log.txt


# remove files..
				#find /imls-fs003/$group/*  -type f >> test_file_log.txt
			else
				 echo [ERROR]  [ ${date_hour} ]  $group had error during copy job! >> ${date}_move_log.txt
			fi

	fi
}


# Loop through each directory and tar it
for directory in `find /imls-fs003/ -maxdepth 1 -type d`
	do
	# define Filename and Directoryname
		group=$(basename $directory)

		#Switch Case for groupspecific min/max depth (greber/folder_to_tar   vs   greber/personXYZ/folder_to_tar)(1 vs 2)
		case $group in
			greber)


				tar_and_copy 1 UG_Archiv
			;;


			stoeckli)
				tar_and_copy 2 ES_Archiv
			;;

			hajnal)
				tar_and_copy 2 AH_Archiv
			;;

			IT)

				tar_and_copy 1 IT_Archiv


			;;
			TestGroup)

				tar_and_copy 2 Test_Archiv


			;;


		esac
	done


echo Tar log from ${date} | mail -s "Tar log ${date}" marco.schmidli@imls.uzh.ch,fabio.snozzi@imls.uzh.ch -A ${date}_tar_log.txt
echo File log from ${date} | mail -s "File log ${date}" marco.schmidli@imls.uzh.ch,fabio.snozzi@imls.uzh.ch -A test_file_log.txt
echo Move log from ${date} | mail -s "Move log ${date}" marco.schmidli@imls.uzh.ch,fabio.snozzi@imls.uzh.ch -A ${date}_move_log.txt
