#!/bin/bash

##########################################
## Script zum löschen und erstellen der ##
## der üblichen UFW Firewall Regeln     ##
## für MLS & DQBM von Marco Schmidli    ##
##########################################

# Color declaration for further use later on
NONE='\033[00m'
RED='\033[01;31m'
GREEN='\033[01;32m'
YELLOW='\033[01;33m'
PURPLE='\033[01;35m'
CYAN='\033[01;36m'
WHITE='\033[01;37m'
BOLD='\033[1m'
UNDERLINE='\033[4m'

# Step 1: Check if Debian or Ubuntu
linuxDistro=$(awk -F= '/^ID=/ {print $2}' /etc/os-release)

# Confirm Distro is correct and depending on the input stop the Script
echo -e "$PURPLE""$linuxDistro detected as OS.$NONE"
read -p "$(echo -e $CYAN"Is this correct? (Y/n) "$NONE)" confirmationOS

#Set y as default if User just pressed Enter
confirmationOS=${confirmationOS:-y}
echo -e "\n"
if [[ $confirmationOS =~ [yY] ]]; then
  echo -e "$GREEN""*** Distro set as $linuxDistro ***$NONE"
else
  read -p "$(echo -e "$CYAN""Enter linux distro in lowercase: "$NONE)" linuxDistro
  echo -e "\n$GREEN""*** Distro set as $linuxDistro ***$NONE"
fi

if [[ $linuxDistro == 'ubuntu' ]]; then
  # Ubuntu code (later maybe Sudo Distros)
  if [ "$EUID" -ne 0 ]; then
    read -p "$(echo -e $RED"
**************************
Please run with sudo or as root.
Press any key to close script.
**************************"$NONE)"
    exit 1
  else
    sudoVariable="sudo"
  fi

elif

  [[ $linuxDistro == 'debian' ]]
then
  # Debian Code
  # Check if Script was run as Sudo or Root
  if [ "$EUID" -ne 0 ]; then
    read -p "$(echo -e $RED"
**************************
Please run with root.
Press any key to close script.
**************************"$NONE)"
    exit 1
  else
    sudoVariable=""
  fi

else
  # Distro is not Debian or Ubuntu -> Abort
  echo -e "$RED""*** Distro is not set to a supported OS ***\nCancelling the Script...$NONE"
  exit 1
fi

#Killing dpkg processes and unattended upgrade service
echo -e "$PURPLE""Killing unattended upgrades service temporarily and killing all dpkg processes to ensure lock for package installs.$NONE"
$sudoVariable systemctl stop unattended-upgrades.service
$sudoVariable pkill dpkg

#Install UFW Package
echo -e "$PURPLE""Installing UFW package...$NONE"
$sudoVariable apt -y install ufw
# Cleanup
$sudoVariable apt -y autoremove

ufwStatus=$($sudoVariable ufw status numbered | awk -F': ' '/^Status:/ {print $2}')
if [[ $ufwStatus == 'active' ]]; then
  echo -e "$GREEN""*** Firewall is currently active ***\n $NONE"
  echo -e "$GREEN""*** Listing current UFW Firewall Status: ***\n $NONE"
  # UFW Regeln auflisten
  $sudoVariable ufw status numbered
  echo -e "\n"
  # Abfrage ob Regeln gelöscht werden sollen
  read -p "$(echo -e "$CYAN""Do you want to delete any existing rule? (y/N) "$NONE)" deleteRuleYN
  # Set default to N
  deleteRuleYN=${deleteRuleYN:-n}
  echo -e "\n"
  if [[ $deleteRuleYN =~ [yY] ]]; then
    read -p "$(echo -e "$CYAN""Enter the Numbers seperated by space of the Firewall Rules you wish to delete.\nStart from the highest number.\n"$NONE)" ufwRulesToDelete
    # Replace , with space (prevent faulty user Input)
    ufwRulesToDeleteSpaces=$(echo "$ufwRulesToDelete" | tr "," " ")
    # Sort input from highest to lowest to prevent accidental Errors
    ufwRulesToDeleteSorted=$(echo "$ufwRulesToDeleteSpaces" | tr " " "\n" | sort -gr)
    # Run ufw delete command for each rule separately with confirmation ( do yes | ufw delete ..) to remove confirmation Step
    for i in $ufwRulesToDeleteSorted; do $sudoVariable ufw delete "$i"; done
    echo -e "$GREEN""*** Done deleting Firewall Rules ***\n $NONE"
  fi
#Display message with Firewall inactive status
else
  echo -e "$RED""*** Firewall is currently inactive ***\n $NONE"
fi

# Abfragen ob neue Regeln hinzugefügt werden sollen
read -p "$(echo -e "$CYAN""Do you want to add any predefined rules? (Y/n) "$NONE)" addRulesYN
# Set default to Y
addRulesYN=${addRulesYN:-y}
echo -e "\n"
if [[ $addRulesYN =~ [yY] ]]; then
  read -p "$(echo -e "$CYAN""Do you want to add a SSH Rule? (Y/n) "$NONE)" addSSHYN
  addSSHYN=${addSSHYN:-y}
  echo -e "\n"
  if [[ $addSSHYN =~ [yY] ]]; then

    # Ask for SSH Port in a loop (check if Port is valid)
    SSHportOK=false
    while [[ $SSHportOK == false ]]; do
      read -p "$(echo -e "$CYAN""Enter the used SSH Port: (44) "$NONE)" SSHPort
      SSHPort=${SSHPort:-44}
      if [[ $SSHPort -ge 0 && $SSHPort -le 65535 ]]; then
        SSHportOK=true
      else
        echo -e "$RED""Please set a correct Port (0 - 65535)!\n$NONE"
      fi
    done

    echo -e "$GREEN""*** SSH Port set to $SSHPort ***\n$NONE"
    # Select Menu for Rule
    read -p "$(echo -e "$CYAN""Please select:\n1) IT Hosts (default)\n2) All\n3) Custom\n"$NONE)" SSHRuleMode
    SSHRuleMode=${SSHRuleMode:-"1"}
    case $SSHRuleMode in
    "1")
      echo -e "$PURPLE""Adding IT Hosts to port $SSHPort\n$NONE"

#      $sudoVariable ufw allow proto tcp from 130.60.106.248 to any port "$SSHPort" comment 'SSH Marco Y13'
#      if [[ $? == 0 ]]; then
#        echo -e "$GREEN""*** Added SSH Rule for Marco Y13 ***\n$NONE"
#      else
#        echo -e "$RED""*** Error adding SSH Rule for Marco Y13 ***\n$NONE"
#      fi
#
#      $sudoVariable ufw allow proto tcp from 130.60.106.142 to any port "$SSHPort" comment 'SSH Fabio Y13'
#      if [[ $? == 0 ]]; then
#        echo -e "$GREEN""*** Added SSH Rule for Fabio Y13' ***\n$NONE"
#      else
#        echo -e "$RED""*** Error adding SSH Rule for Fabio Y13' ***\n$NONE"
#      fi
#      $sudoVariable ufw allow proto tcp from 130.60.106.143 to any port "$SSHPort" comment 'SSH Y13 (Mac)'
#      if [[ $? == 0 ]]; then
#        echo -e "$GREEN""*** Added SSH Rule for Y13 (Mac) ***\n$NONE"
#      else
#        echo -e "$RED""*** Error adding SSH Rule for Y13 (Mac) ***\n$NONE"
#      fi
#      $sudoVariable ufw allow proto tcp from 130.60.106.191 to any port "$SSHPort" comment 'SSH Y13 (JumpHost)'
#      if [[ $? == 0 ]]; then
#        echo -e "$GREEN""*** Added SSH Rule for Y13 (JumpHost) ***\n$NONE"
#      else
#        echo -e "$RED""*** Error adding SSH Rule for Y13 (JumpHost) ***\n$NONE"
#      fi

#      $sudoVariable ufw allow proto tcp from 130.60.129.200/29 to any port "$SSHPort" comment 'SSH Fabio & Marco Y32'
#      if [[ $? == 0 ]]; then
#        echo -e "$GREEN""*** Added SSH Rule for Fabio & Marco Y32 ***\n$NONE"
#      else
#        echo -e "$RED""*** Error adding SSH Rule for Fabio & Marco Y32 ***\n$NONE"
#      fi


      $sudoVariable ufw allow proto tcp from 10.65.251.16/28 to any port "$SSHPort" comment 'SSH IT (und Server) NAC'
      if [[ $? == 0 ]]; then
        echo -e "$GREEN""*** Added SSH Rule for SSH IT (und Server) NAC ***\n$NONE"
      else
        echo -e "$RED""*** Error adding SSH Rule for SSH IT (und Server) NAC ***\n$NONE"
      fi

#      $sudoVariable ufw allow proto tcp from 130.60.93.12 to any port "$SSHPort" comment 'SSH Y55 (FW)'
#      if [[ $? == 0 ]]; then
#        echo -e "$GREEN""*** Added SSH Rule for Y55 (FW) ***\n$NONE"
#      else
#        echo -e "$RED""*** Error adding SSH Rule for Y55 (FW) ***\n$NONE"
#      fi
#      $sudoVariable ufw allow proto tcp from 130.60.93.71 to any port "$SSHPort" comment 'SSH Werner Y55 (Mac)'
#      if [[ $? == 0 ]]; then
#        echo -e "$GREEN""*** Added SSH Rule for Werner Y55 (Mac) ***\n$NONE"
#      else
#        echo -e "$RED""*** Error adding SSH Rule for Werner Y55 (Mac) ***\n$NONE"
#      fi
#      $sudoVariable ufw allow proto tcp from 130.60.93.131 to any port "$SSHPort" comment 'SSH Werner Y55 (JumpHost)'
#      if [[ $? == 0 ]]; then
#        echo -e "$GREEN""*** Added SSH Rule for Werner Y55 (JumpHost) ***\n$NONE"
#      else
#        echo -e "$RED""*** Error adding SSH Rule for Werner Y55 (JumpHost) ***\n$NONE"
#      fi

#      TODO: Add Thomas IP
#      $sudoVariable ufw allow proto tcp from 130.60.93.xx to any port "$SSHPort" comment 'SSH Thomas Y55 (Mac)'
#      if [[ $? == 0 ]]; then
#        echo -e "$GREEN""*** Added SSH Rule for Thomas Y55 (Mac) ***\n$NONE"
#      else
#        echo -e "$RED""*** Error adding SSH Rule for Thomas Y55 (Mac) ***\n$NONE"
#      fi
#      $sudoVariable ufw allow proto tcp from 130.60.93.xx to any port "$SSHPort" comment 'SSH Thomas Y55 (PC)'
#      if [[ $? == 0 ]]; then
#        echo -e "$GREEN""*** Added SSH Rule for Thomas Y55 (PC) ***\n$NONE"
#      else
#        echo -e "$RED""*** Error adding SSH Rule for Thomas Y55 (PC) ***\n$NONE"
#      fi
      ;;
    "2")
      echo -e "$PURPLE""Adding Anywhere SSH Rule to port $SSHPort\n$NONE"
      $sudoVariable ufw allow proto tcp from 0.0.0.0/0 to any port "$SSHPort" comment 'SSH from Anywhere'
      if [[ $? == 0 ]]; then
        echo -e "$GREEN""*** Added SSH from anywhere Rule ***\n$NONE"
      else
        echo -e "$RED""*** Error adding SSH from anywhere Rule ***\n$NONE"
      fi
      ;;
    "3")
      # While loop to repeat adding Hosts / Subnets
      AddMoreSSHRules=true
      while [[ $AddMoreSSHRules == true ]]; do
        # Get Host/Subnet and Comment from UserInput
        read -p "$(echo -e "$CYAN""Please enter a SINGLE host (e.g. 10.10.10.10) or subnet (10.10.10.0/24): "$NONE)" AddSSHHost
        read -p "$(echo -e "$CYAN""Please enter a comment for this Rule: "$NONE)" AddSSHComment
        # Add the Rule in UFW
        $sudoVariable ufw allow proto tcp from "$AddSSHHost" to any port "$SSHPort" comment "$AddSSHComment"
        echo -e "$GREEN""*** Added SSH Rule for ""$AddSSHComment"" ***\n$NONE"
        # Interrupt the While Loop if no more Hosts / Subnets need to be added
        read -p "$(echo -e "$CYAN""Do you want to add another Host or Subnet? (Y/n) "$NONE)" AddMoreSSHRulesYN
        AddMoreSSHRulesYN=${AddMoreSSHRulesYN:-y}
        if ! [[ $AddMoreSSHRulesYN =~ [yY] ]]; then
          AddMoreSSHRules=false
        fi
      done
      ;;
    *)
      echo -e "$RED""Wrong input! - Skipping SSH Rule\n$NONE"
      ;;
    esac
  fi

  # CheckMK Rules
  read -p "$(echo -e "$CYAN""Do you want to add CheckMK Rules? (Y/n) "$NONE)" addCheckMKYN
  addCheckMKYN=${addCheckMKYN:-y}
  echo -e "\n"
  if [[ $addCheckMKYN =~ [yY] ]]; then
    # If the server added any SSH Rule ask for a SSH Rule for CheckMK Server
    if [[ $addSSHYN =~ [yY] ]]; then
      read -p "$(echo -e "$CYAN""Do you want to add SSH Rule for CheckMK? (Y/n) "$NONE)" addSSHCheckMK
      addSSHCheckMK=${addSSHCheckMK:-y}
      if [[ $addSSHCheckMK =~ [yY] ]]; then
        $sudoVariable ufw allow proto tcp from 130.60.145.10 to any port "$SSHPort" comment 'SSH CheckMK Server'
        if [[ $? == 0 ]]; then
          echo -e "$GREEN""*** Added SSH Rule for CheckMK Server ***\n$NONE"
        else
          echo -e "$RED""*** Error adding SSH Rule for CheckMK Server ***\n$NONE"
        fi
      fi
    fi
    # Ask for CheckMK Port in a loop (check if Port is valid)
    CheckMKportOK=false
    while [[ $CheckMKportOK == false ]]; do
      read -p "$(echo -e "$CYAN""Enter the used CheckMK Port: (6556) "$NONE)" CheckMKPort
      CheckMKPort=${CheckMKPort:-6556}
      if [[ $CheckMKPort -ge 0 && $CheckMKPort -le 65535 ]]; then
        CheckMKportOK=true
      else
        echo -e "$RED""Please set a correct Port (0 - 65535)!\n$NONE"
      fi
    done
    # Add CheckMK Agent Rule
    echo -e "$GREEN""*** SSH Port set to $CheckMKPort ***\n$NONE"
    $sudoVariable ufw allow proto tcp from 130.60.145.10 to any port "$CheckMKPort" comment 'CheckMK Agent'
    if [[ $? == 0 ]]; then
      echo -e "$GREEN""*** Added CheckMK Agent Rule for CheckMK Server ***\n$NONE"
    else
      echo -e "$RED""*** Error adding CheckMK Agent Rule for CheckMK Server ***\n$NONE"
    fi
  fi

  # HTTP / HTTPS Rules
  read -p "$(echo -e "$CYAN""Do you want to add HTTP / HTTPS Rules (letsencrypt)? (y/N) "$NONE)" addHTTPSYN
  addHTTPSYN=${addHTTPSYN:-n}
  echo -e "\n"
  if [[ $addHTTPSYN =~ [yY] ]]; then
    # Select Menu for Rule
    read -p "$(echo -e "$CYAN""Please select:\n1) HTTP / HTTPS for All (letsencrypt) (default)\n2) HTTPS for all\n"$NONE)" HTTPSRuleMode
    HTTPSRuleMode=${HTTPSRuleMode:-"1"}
    case $HTTPSRuleMode in
    "1")
      $sudoVariable ufw allow proto tcp from any to 0.0.0.0/0 port 80 comment 'HTTP (letsencrypt)'
      if [[ $? == 0 ]]; then
        echo -e "$GREEN""*** Added HTTP Rule ***\n$NONE"
      else
        echo -e "$RED""*** Error adding HTTP Rule ***\n$NONE"
      fi
      $sudoVariable ufw allow proto tcp from any to 0.0.0.0/0 port 443 comment 'HTTPS (letsencrypt)'
      if [[ $? == 0 ]]; then
        echo -e "$GREEN""*** Added HTTPS Rule ***\n$NONE"
      else
        echo -e "$RED""*** Error adding HTTPS Rule ***\n$NONE"
      fi
      ;;
    "2")
      HTTPSportOK=false
      while [[ $HTTPSportOK == false ]]; do
        read -p "$(echo -e "$CYAN""Please enter the used SSH Port (443): "$NONE)" HTTPSPort
        HTTPSPort=${HTTPSPort:-"443"}
        if [[ $HTTPSPort -ge 0 && $HTTPSPort -le 65535 ]]; then
          HTTPSportOK=true
        else
          echo -e "$RED""Please set a correct Port (0 - 65535)!\n$NONE"
        fi
      done
      echo -e "$GREEN""*** HTTPS Port set to ""$HTTPSPort"" ***\n$NONE"
      $sudoVariable ufw allow proto tcp from 0.0.0.0/0 to any port "$HTTPSPort" comment 'HTTPS from any'
      if [[ $? == 0 ]]; then
        echo -e "$GREEN""*** Added HTTPS Rule ***\n$NONE"
      else
        echo -e "$RED""*** Error adding HTTPS Rule ***\n$NONE"
      fi
      ;;
    *)
      echo -e "$RED""*** Error Skipping this ***\n$NONE"
      ;;
    esac
  fi
fi

moreUFWRules=true
while [[ $moreUFWRules == true ]]; do
  read -p "$(echo -e "$CYAN""Do you want to add more custom rules? (y/N) "$NONE)" moreRules
  moreRules=${moreRules:-n}
  if [[ $moreRules =~ [yY] ]]; then
    CustomPortOK=false

    while [[ $CustomPortOK == false ]]; do
      read -p "$(echo -e "$CYAN""Please enter the used Port (and protocol) (1234 tcp OR 1234): "$NONE)" CustomPortProto
      # Change , into spaces (if wrong userinput)
      CustomPortProto=$(echo "$CustomPortProto" | tr "," " ")

      # Split input on Spaces into Port & proto variable
      CustomPort=$(echo "$CustomPortProto" | cut -f1 -d" ")
      proto=$(echo "$CustomPortProto" | cut -f2 -d" ")

      # Small "Hack" to check if protocol was set or not
      if [[ $proto == $CustomPort ]]; then
        proto=""
      fi

      # Check if Port and protocol is OK
      if [[ $CustomPort -ge 0 && $CustomPort -le 65535 && $proto =~ ([uU][dD][pP])|([tT][cC][pP]|^$) ]]; then
        CustomPortOK=true
      else
        echo -e "$RED""Please set a correct Port (0 - 65535)!\n$NONE"
      fi
    done
    read -p "$(echo -e "$CYAN""Please enter a SINGLE host (e.g. 10.10.10.10) or subnet (10.10.10.0/24): "$NONE)" AddCustomHost
    read -p "$(echo -e "$CYAN""Please enter a comment for this Rule: "$NONE)" AddCustomComment
    # Add the Rule in UFW
    if [[ $proto == "" ]]; then
      # Generic Rule (TCP & UDP Protocol OK)
      $sudoVariable ufw allow from "$AddCustomHost" to any port "$CustomPort" comment "$AddCustomComment"
      echo -e "$GREEN""*** Added Rule for ""$AddCustomComment"" ***\n$NONE"
    else
      # Rule with specified Protocol
      $sudoVariable ufw allow proto "${proto,,}" from "$AddCustomHost" to any port "$CustomPort" comment "$AddCustomComment"
      echo -e "$GREEN""*** Added Rule for ""$AddCustomComment"" ***\n$NONE"
    fi

  else
    moreUFWRules=false
    echo -e "$GREEN""*** Finished setting Rules ***\n$NONE"
  fi
done

if [[ $ufwStatus == 'inactive' ]]; then
  read -p "$(echo -e "$CYAN""Do you want to activate the rules now? (Y/n) "$NONE)" activateRules
  activateRules=${activateRules:-y}
  if [[ $activateRules =~ [yY] ]]; then
    yes | $sudoVariable ufw enable
    if [[ $? == 0 ]]; then
      echo -e "$PURPLE""\nListing current rules\n$NONE"
      ufw status numbered
      echo -e "$GREEN""\n*** Finished Script ***\n$NONE"
    fi
  fi
else
  echo -e "$PURPLE""\nListing current rules\n$NONE"
  $sudoVariable ufw status numbered
  echo -e "$GREEN""\n*** Finished Script ***\n$NONE"
fi
