#!/bin/bash

##########################################
## Script um Linux Rechner dem Active   ##
## Directory der UZH hinzuzufügen       ##
## für MLS & DQBM von Marco Schmidli    ##
##########################################

# Color declaration for further use later on
NONE='\033[00m'
RED='\033[01;31m'
GREEN='\033[01;32m'
YELLOW='\033[01;33m'
PURPLE='\033[01;35m'
CYAN='\033[01;36m'
WHITE='\033[01;37m'
BOLD='\033[1m'
UNDERLINE='\033[4m'

# Step 1: Check if Debian or Ubuntu
linuxDistro=$(awk -F= '/^ID=/ {print $2}' /etc/os-release)

# Confirm Distro is correct and depending on the input stop the Script
echo -e "$PURPLE""$linuxDistro detected as OS.$NONE"
read -p "$(echo -e $CYAN"Is this correct? (Y/n) "$NONE)" confirmationOS

#Set y as default if User just pressed Enter
confirmationOS=${confirmationOS:-y}
echo -e "\n"
if [[ $confirmationOS =~ [yY] ]]; then
  echo -e "$GREEN""*** Distro set as $linuxDistro ***$NONE"
else
  read -p "$(echo -e "$CYAN""Enter linux distro in lowercase: "$NONE)" linuxDistro
  echo -e "\n$GREEN""*** Distro set as $linuxDistro ***$NONE"
fi

if [[ $linuxDistro == 'ubuntu' ]]; then
  # Ubuntu code (later maybe Sudo Distros)
  if [ "$EUID" -ne 0 ]; then
    read -p "$(echo -e $RED"
**************************
Please run with sudo or as root.
Press any key to close script.
**************************"$NONE)"
    exit 1
  else
    sudoVariable="sudo"
  fi

elif

  [[ $linuxDistro == 'debian' ]]
then
  # Debian Code
  # Check if Script was run as Sudo or Root
  if [ "$EUID" -ne 0 ]; then
    read -p "$(echo -e $RED"
**************************
Please run with root.
Press any key to close script.
**************************"$NONE)"
    exit 1
  else
    sudoVariable=""
  fi

else
  # Distro is not Debian or Ubuntu -> Abort
  echo -e "$RED""*** Distro is not set to a supported OS ***\nCancelling the Script...$NONE"
  exit 1
fi

# Loop to check & edit hostname
hostnameCorrect=false
while [[ $hostnameCorrect == false ]]; do
  shorthost=$(hostname)
  read -p "$(echo -e "$CYAN""Is this hostname correct: ($shorthost)  (Y/n) $NONE")" hostnameYN
  hostnameYN=${hostnameYN:-y}
  if [[ $hostnameYN =~ [nN] ]]; then
    read -p "$(echo -e "$CYAN""Enter the new hostname (without domain part): $NONE")" newHostname
    #Set new hostname via hostnamectl
    $sudoVariable hostnamectl set-hostname $newHostname
    #Replace 127.0.0.1 Entry in the hosts file with new hostname
    $sudoVariable cp /etc/hosts /etc/hosts.bkup
    # Replace all 127.0.0.1 entries with empty space
    sed -i "/127.0.0.1/c\ " /etc/hosts
    # insert new 127.0.0.1 entry
    echo "127.0.0.1 $newHostname" >>/etc/hosts

  else
    hostnameCorrect=true
  fi
done

#Check DNS Entry
echo -e "$PURPLE""Current DNS Config:\n"
cat /etc/resolv.conf
echo -e $NONE

ALLDNS=$(awk -F" " '/^nameserver/ {print $2}' /etc/resolv.conf)
ALLDNS=$(echo "$ALLDNS" | tr "\n" " ")

DNS1=$(echo "$ALLDNS" | cut -f1 -d" ")
DNS2=$(echo "$ALLDNS" | cut -f2 -d" ")
DNS3=$(echo "$ALLDNS" | cut -f3 -d" ")

if [[ $DNS1 != 130.60.215.56 && $DNS1 != 130.60.215.57 ]]; then
  read -p "$(echo -e "$CYAN""Do you want to change your DNS to 130.60.215.56/57? (Y/n): $NONE")" changeDNSYN
  changeDNSYN=${changeDNSYN:-y}
  if [[ $changeDNSYN =~ [yY] ]]; then
    echo -e "$PURPLE""Creating backup of current DNS config:\n$NONE"
    # Create copy of original file
    cp /etc/resolv.conf /etc/resolv.conf.bkup
    # Replace all nameserver entries with empty space
    sed -i "/nameserver/c\ " /etc/resolv.conf
    # insert new nameserver entries
    echo "nameserver 130.60.215.56" >>/etc/resolv.conf
    echo "nameserver 130.60.215.57" >>/etc/resolv.conf
    echo -e "$GREEN""Current DNS Config:\n"
    cat /etc/resolv.conf
    echo -e $NONE
  fi
fi

# Checking Timesync settings
timeSyncStatus=$($sudoVariable systemctl status systemd-timesyncd | awk -F': ' '/Active: / {print $2}')
echo "Timesync:"
echo $timeSyncStatus

if [[ $timeSyncStatus =~ ^inactive.* ]]; then
  echo -e "$PURPLE""Timesync seems to be inactive$NONE"

  #Replace NTP with time.uzh.ch
  sed -i "/[#?]NTP.*/c\NTP=time.uzh.ch" /etc/systemd/timesyncd.conf
  # Set default Fallback NTP's
  sed -i "/[#?]FallbackNTP.*/c\FallbackNTP=0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org" /etc/systemd/timesyncd.conf

  $sudoVariable systemctl enable systemd-timesyncd
  $sudoVariable systemctl start systemd-timesyncd
  echo -e "$GREEN""*** Changed Timesync Server settings ***\n$NONE"
  $sudoVariable systemctl status systemd-timesyncd
else
  timeSyncServer=$($sudoVariable cat /etc/systemd/timesyncd.conf | grep -E '^[#]?NTP=.*' | cut -f2 -d"=")

  if [[ $timeSyncServer != time.uzh.ch ]]; then
    echo -e "$PURPLE""Current TimesyncServer: $timeSyncServer""$NONE"
    read -p "$(echo -e "$CYAN""Do you want to change your Timesync Settings to time.uzh.ch? (Y/n): $NONE")" changeTimeSyncYN
    changeTimeSyncYN=${changeTimeSyncYN:-y}
    if [[ $changeTimeSyncYN =~ [yY] ]]; then
      #Replace NTP with time.uzh.ch
      sed -i "/#?NTP.*/c\NTP=time.uzh.ch" /etc/systemd/timesyncd.conf
      # Set default Fallback NTP's
      sed -i "/#?FallbackNTP.*/c\FallbackNTP=0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org" /etc/systemd/timesyncd.conf
      $sudoVariable systemctl enable systemd-timesyncd
      $sudoVariable systemctl restart systemd-timesyncd
      echo -e "$GREEN""*** Changed Timesync Server settings ***\n$NONE"
      $sudoVariable systemctl status systemd-timesyncd
    fi
  fi
fi

#Killing dpkg processes and unattended upgrade service
echo -e "$PURPLE""Killing unattended upgrades service temporarily and killing all dpkg processes to ensure lock for package installs.$NONE"
$sudoVariable systemctl stop unattended-upgrades.service
$sudoVariable pkill dpkg

echo -e "$PURPLE""Installing necessary packages...$NONE"
$sudoVariable apt -y install realmd libnss-sss libpam-sss sssd sssd-tools adcli samba-common-bin krb5-user oddjob oddjob-mkhomedir packagekit

echo -e "$GREEN""*** Completed installation of necessary packages ***.\n$PURPLE""Now printing discovered Kerberos realms...$NONE"
echo "Default:"
realm discover

echo -e "$PURPLE""Searching for d.uzh.ch:$NONE"
realm discover d.uzh.ch

echo -e "$PURPLE""Your domain and its properties should be printed above. If they are not, check DNS config.$NONE"
read -p "$(echo -e "$CYAN""What is the Kerberos realm? (D.UZH.CH)? $NONE")" REALMAD
REALMAD=${REALMAD:-"D.UZH.CH"}
read -p "$(echo -e "$CYAN""What is the domain controllers short hostname ? (IDUZHZ2DC01.d.uzh.ch) $NONE")" REALMDC
REALMDC=${REALMDC:-"IDUZHZ2DC01.d.uzh.ch"}
read -p "$(echo -e "$CYAN""What is the domain admin username? $NONE")" REALMADMIN

echo -e "$PURPLE""Performing domain join operation. Password for domain admin will be prompted.$NONE"
$sudoVariable realm join -v -U "$REALMADMIN" "$REALMDC"

echo -e "$PURPLE""Activating mkhomedir module...$NONE"
echo 'Name: activate mkhomedir
Default: yes
Priority: 900
Session-Type: Additional
Session:
        required  pam_mkhomedir.so umask=0022 skel=/etc/skel' | $sudoVariable tee /usr/share/pam-configs/mkhomedir
$sudoVariable pam-auth-update --enable mkhomedir
$sudoVariable systemctl restart sssd

## Make the domain the default login domain for the login screen. Simplifies logins.
#$sudoVariable sed -i "/sssd/a default_domain_suffix = $REALMAD" /etc/sssd/sssd.conf


#Replace NTP with time.uzh.ch
$sudoVariable sed -i "/access_provider.*/c\access_provider=simple" /etc/sssd/sssd.conf
$sudoVariable sed -i "/fallback_homedir.*/c\fallback_homedir=/home/%u" /etc/sssd/sssd.conf
$sudoVariable sed -i "/use_fully_qualified_names.*/c\use_fully_qualified_names=False" /etc/sssd/sssd.conf
$sudoVariable sed -i "/ldap_id_mapping.*/c\ldap_id_mapping=True" /etc/sssd/sssd.conf

#fix SSSD Config

#Does ldap_referrals exist in config file?
if grep -Fq 'ldap_referrals' /etc/sssd/sssd.conf
then
#Delete all entries with ldap referrals
  $sudoVariable sed -i "/ldap_referrals.*/d" /etc/sssd/sssd.conf
#Add ldap_referrals = False
  echo "ldap_referrals=False" >>/etc/sssd/sssd.conf
else
 echo "ldap_referrals=False" >>/etc/sssd/sssd.conf
fi

#Does ignore_group_members  exist in config file?
if grep -Fq 'ignore_group_members' /etc/sssd/sssd.conf
then
#Delete all entries with ignore_group_members
  $sudoVariable sed -i "/ignore_group_members.*/d" /etc/sssd/sssd.conf
#Add ignore_group_members  = True
  echo "ignore_group_members=True" >>/etc/sssd/sssd.conf
else
 echo "ignore_group_members=True" >>/etc/sssd/sssd.conf
fi



$sudoVariable systemctl restart sssd

if [[ $linuxDistro == 'ubuntu' ]]; then
  sudocheck=0
  while [ "$sudocheck" -ne 1 ]; do
    read -p "$(echo -e "$CYAN""Add a domain user to local sudoers? Y/N $NONE")" sudoinput
    if [[ "$sudoinput" =~ ^([yY][eE][sS]|[yY])+$ ]]; then
      read -p "$(echo -e "$CYAN""Alright! What's the username? Exclude the @$REALMAD part.$NONE")" sudoun
      echo -e "$PURPLE""Adding $sudoun@$REALMAD to /etc/sudoers.d directory..$NONE"
      echo "$sudoun ALL=(ALL:ALL) ALL" | $sudoVariable tee /etc/sudoers.d/$sudoun
      $sudoVariable chown root:root /etc/sudoers.d/$sudoun
      $sudoVariable chmod 440 /etc/sudoers.d/$sudoun
      $sudoVariable usermod -aG sudo "$sudoun@$REALMAD"
      echo -e "$GREEN""*** Done adding user $sudoun@$REALMAD ***$NONE"

    elif [[ "$sudoinput" =~ ^([nN][oO]|[nN])+$ ]]; then
      echo -e "$PURPLE""Alright, moving on.$NONE"
      sudocheck=1

    else
      echo -e "$RED""That input doesn't make sense. Please try again.$NONE"
    fi
  done
fi
#prompt
read -r -p "$(echo -e "$CYAN""Domain Join Complete! REBOOT NOW? [y/N] $NONE")" rebootnow
if [[ "$rebootnow" =~ ^([yY][eE][sS]|[yY])+$ ]]; then
  echo -e "$PURPLE""Rebooting!$NONE"
  $sudoVariable reboot
else
  read -p "$(echo -e "$PURPLE""Reboot not selected. Press any key to finish the script.$NONE")"
fi
